# RedisPlus

#### 项目介绍
RedisPlus是为Redis管理开发的桌面客户端软件，支持Windows 、Linux 、Mac三大系统平台，RedisPlus提供更加高效、方便、快捷的使用体验，有着更加现代化的界面风格。该软件参考了RedisStudio的界面逻辑，但是和RedisStudio软件完全没有关系，并不是官方推出的跨平台软件。该软件为开源免费使用（可商用），但是禁止二次开发打包发布盈利，违反必究！ 该软件遵循Apache License 2.0开源协议，致力于为大家提供一个高效的Redis可视化管理软件。
 

#### 技术选型</br>
1.支持跨平台，使用java开发</br>
2.使用javafx的桌面元素</br>
3.使用derby内嵌数据库</br>
4.内置服务使用springboot开发</br>

应用截图</br>

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174302_dfd839b5_1252126.png "深度截图_com.maxbill.MainApplication_20180904174001.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174311_9991de81_1252126.png "深度截图_com.maxbill.MainApplication_20180904174021.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174321_53591727_1252126.png "深度截图_com.maxbill.MainApplication_20180904174037.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174331_3baf9be8_1252126.png "深度截图_com.maxbill.MainApplication_20180904174051.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174341_dcfb9cb9_1252126.png "深度截图_com.maxbill.MainApplication_20180904174106.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174352_3738f4fe_1252126.png "深度截图_com.maxbill.MainApplication_20180904174116.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174402_2b5060d0_1252126.png "深度截图_com.maxbill.MainApplication_20180904174134.png")


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)